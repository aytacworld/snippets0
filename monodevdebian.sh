#!/bin/sh

apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF

echo "deb http://download.mono-project.com/repo/debian wheezy main" | tee /etc/apt/sources.list.d/mono-xamarin.list

apt-get update

apt-get install -y --force-yes mono-devel mono-complete referenceassemblies-pcl

certmgr -ssl -m https://go.microsoft.com 
certmgr -ssl -m https://nugetgallery.blob.core.windows.net 
certmgr -ssl -m https://nuget.org
certmgr -ssl -m https://www.mynet.org/F/aspnetvnext/ 
mozroots --import --machine --sync

apt-get install -y --force-yes xorg lxde-core lightdm

apt-get install -y --force-yes monodevelop monodevelop-nunit monodevelop-versioncontrol monodevelop-database

echo "Create .Net development Environment on LinuxDebian done"

echo "Install Leafpad?"
apt-get install leafpad

echo "Install IceWeasel(Firefox)?"
apt-get install iceweasel

echo "Install Midori?"
apt-get install midori

echo "Install Chromium(Chrome)?"
apt-get install chromium

echo "Install Alsa-Utils(Sound)?"
apt-get install alsa-utils

echo "Install rfkill(wifi enable/disable)?"
apt-get install rfkill