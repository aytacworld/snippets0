#!/bin/bash

clear

sudo apt-get update
sudo apt-get upgrade

sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
echo "deb http://download.mono-project.com/repo/debian wheezy main" | sudo tee /etc/apt/sources.list.d/mono-xamarin.list

sudo apt-get update

sudo apt-get install make
sudo apt-get install git autoconf libtool automake build-essential mono-devel gettext zip unzip
sudo apt-get install bash zsh curl gyp
sudo apt-get install mono-complete

sudo certmgr -ssl -m https://go.microsoft.com
sudo certmgr -ssl -m https://nugetgallery.blob.core.windows.net
sudo certmgr -ssl -m https://nuget.org
sudo certmgr -ssl -m https://www.myget.org/F/aspnetvnext/

mozroots --import –sync

wget http://dist.libuv.org/dist/v1.4.2/libuv-v1.4.2.tar.gz 
tar -xvf libuv-v1.4.2.tar.gz
cd libuv-v1.4.2/
./gyp_uv.py -f make -Duv_library=shared_library
make -C out
sudo cp out/Debug/lib.target/libuv.so /usr/lib/libuv.so.1.4.2
sudo ln -s libuv.so.1.4.2 /usr/lib/libuv.so.1

curl -sSL https://raw.githubusercontent.com/aspnet/Home/master/kvminstall.sh | sh && source ~/.k/kvm/kvm.sh

kvm upgrade

mono --version
k --version
kpm --version
