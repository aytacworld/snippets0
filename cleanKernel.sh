#!/bin/sh

echo "All Kernels"

dpkg -l linux-image-\* | grep ^ii

echo "Unused old kernel"

kernelver=$(uname -r | sed -r 's/-[a-z]+//')
dpkg -l linux-{image,headers}-"[0-9]*" | awk '/ii/{print $2}' | grep -ve $kernelver

echo "Deleting old kernels"

apt-get purge $(dpkg -l linux-{image,headers}-"[0-9]*" | awk '/ii/{print $2}' | grep -ve "$(uname -r | sed -r 's/-[a-z]+//')")

echo "Deletion complete"
